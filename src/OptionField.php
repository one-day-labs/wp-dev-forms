<?php

namespace ODL;

/**
 * Class OptionElement
 * @package ODL
 */
class OptionField extends Field {

	/**
	 * An array of options that can be selected
	 * @var array
	 */
	protected $options;

	/**
	 * OptionElement constructor.
	 *
	 * @param $label
	 * @param $name
	 * @param array $options
	 * @param array|null $props
	 */
	public function __construct( $label, $name, array $options, array $props = null ) {
		$this->options = $options;
		if ( ! empty( $this->options ) && array_values( $this->options ) === $this->options ) {
			$this->options = array_combine( $this->options, $this->options ); // ?????
		}

		parent::__construct( $label, $name, $props );
	}

	/**
	 * get the value of a specific option
	 * @param $value
	 *
	 * @return false|string
	 */
	protected function getOptionValue( $value ) {
		$position = strpos( $value, ':pfbc' );
		if ( false === $position ) {
			return $value;
		}

		return ( 0 === $position ) ? '' : substr( $value, 0, $position );
	}
}