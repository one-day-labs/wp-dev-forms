<?php

namespace ODL\Field;

use ODL\Field;

/**
 * Class File
 * @package ODL\Field
 */
class File extends Field {

	/**
	 * Default attributes
	 * @var array
	 */
	protected $_attributes = [ 'type' => 'file' ];
}