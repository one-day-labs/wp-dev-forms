<?php

namespace ODL;

/**
 * Class View
 * @package ODL
 */
abstract class View extends Base {

	/**
	 * The Form that this view is attached to
	 * @var Form
	 */
	protected $_form;

	/**
	 * View constructor.
	 *
	 * @param array|null $props
	 */
	public function __construct( array $props = null ) {
		$this->configure( $props );
	}

	/**
	 * Adds the Form that this view is attached to
	 * @param Form $form
	 */
	public function _setForm( Form $form ) {
		$this->_form = $form;
	}

	/**
	 * Force any classes that extend this view to set a render method
	 */
	public function render() {}

	/**
	 * Force any classes that extend this view to set a renderLabel method
	 *
	 * @param Field $field
	 */
	protected function renderLabel( Field $field ) {}

	/**
	 * Displays the output of any descriptions set on an element
	 * @param $element
	 */
	protected function renderDescription( $element ) {
		$description = $element->getDescription();
		if ( ! empty( $description ) ) {
			echo '<span class="help-inline">', esc_html( $description ), '</span>';
		}
	}
}
