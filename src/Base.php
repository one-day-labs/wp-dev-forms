<?php

namespace ODL;

/**
 * Class Base
 * @package ODL
 */
abstract class Base {

	/**
	 * Allows us to configure the form element
	 * @param array|null $props
	 *
	 * @return $this
	 */
	public function configure( array $props = null ) {
		if ( empty( $props ) ) {
			return $this;
		}

		// Get the name of the class that is being configured
		$class = get_class( $this );

		// Fetch all the variables set in the extended class
		$properties = [];
		foreach ( array_keys( get_class_vars( $class ) ) as $prop ) {
			$properties[ strtolower( $prop ) ] = $prop;
		}

		// Fetch all the methods set on the extended class
		$methods = [];
		foreach ( get_class_methods( $class ) as $method ) {
			$methods[ strtolower( $method ) ] = $method;
		}

		foreach ( $props as $prop => $value ) {
			$prop = strtolower( $prop );

			// Ignore hidden elements (aka the start with underscore – "_")
			if ( '_' === $prop[0] ) {
				continue;
			}

			if ( isset( $methods[ 'set' . $prop ] ) ) {
				$this->{$methods[ 'set'. $prop ]}( $value );
			} else if ( isset( $properties[ $prop ] ) ) {
				$this->{$properties[ $prop ]} = $value;
			} else {
				$this->setAttribute( $prop, $value );
			}
		}

		return $this;
	}

	/**
	 * Fetches all the attributes on an object and returns them in the HTML format
	 * @param string $ignore Optionally ignore specific attributes if needed
	 *
	 * @return string
	 */
	public function getAttributes( $ignore = '' ) {
		$str = '';

		// If no attributes found, return an empty string
		if ( empty( $this->_attributes ) ) {
			return $str;
		}

		if ( ! is_array( $ignore ) ) {
			$ignore = [ $ignore ];
		}

		$atts = array_diff( array_keys( $this->_attributes ), $ignore );
		foreach ( $atts as $att ) {
			$str .= ' ' . $this->sanitize_attr_name( $att );

			if ( '' !== $this->_attributes[ $att ] ) {
				if ( 'class' === $att ) {
					$str .= '="' . sanitize_html_class( $this->_attributes[ $att ] ) . '"';
				} else {
					$str .= '="' . esc_attr( $this->_attributes[ $att ] ) . '"';
				}
			}
		}

		return $str;
	}

	/**
	 * Simple getter method for getting a single attribute on the object
	 * @param $att
	 *
	 * @return string
	 */
	public function getAttribute( $att ) {
		$value = '';

		if ( isset( $this->_attributes[ $att ] ) ) {
			$value = $this->_attributes[ $att ];
		}

		return $value;
	}

	/**
	 * Sets a single attribute to the object
	 * @param $att
	 * @param $value
	 */
	public function setAttribute( $att, $value ) {
		if ( isset( $this->_attributes ) ) {
			$this->_attributes[ $att ] = $value;
		}
	}

	/**
	 * Appends or adds a single attribute to any existing attributes that are on the object
	 * @param $att
	 * @param $value
	 */
	public function appendAttribute( $att, $value ) {
		if ( ! isset( $this->_attribute ) ) {
			return;
		}

		// If there are already attributes assigned, concatenate our new attribute
		if ( ! empty( $this->_attributes[ $att ] ) ) {
			$this->_attributes[ $att ] .= ' ' . $value;
		} else {
			$this->_attributes[ $att ] = $value;
		}
	}

	/**
	 * Since WordPress doesn't appear to provide any data validation helpers for dynamic attribute names, we'll add one.
	 * @param string $name The name of the attribute to be cleaned
	 *
	 * @return string|string[]|null
	 */
	public function sanitize_attr_name( $name ) {
		return is_string( $name ) ? preg_replace( '/[^\p{L}0-9_.-]/', '', $name ) : '';
	}
}
