<?php

namespace ODL;

/**
 * Class Element
 * @package ODL
 */
abstract class Field extends Base implements \Countable{

	/**
	 * The form object this element will be attached to
	 * @var Form
	 */
	protected $_form;

	/**
	 * All the attributes that exist on this element
	 * @var array
	 */
	protected $_attributes = [ 'class' => 'form-control' ];

	/**
	 * The label for this element
	 * @var string
	 */
	protected $label;

	/**
	 * An array of validation objects
	 * @var array
	 */
	protected $validation = [];

	/**
	 * Element description
	 * @var string
	 */
	protected $description;

	/**
	 * Element constructor.
	 *
	 * @param $label
	 * @param $name
	 * @param array|null $props
	 */
	public function __construct( $label, $name, array $props = null ) {
		$configuration = [
			'label' => $label,
			'name' => $name,
		];

		if ( is_array( $props ) ) {
			$configuration = wp_parse_args( $configuration, $props );
		}

		$this->configure( $configuration );
	}

	/**
	 * When an element is serialized and stored in the session, this method prevents an non-essential information from
	 * being included.
	 * @return array
	 */
	public function __sleep() {
		return [
			'_attributes',
			'label',
			'validation',
			'prefillAfterValidation',
		];
	}

	/**
	 * Configure this object to be Countable
	 * @return int
	 */
	public function count() {
		return 1;
	}

	/**
	 * Sets the Form object to the element instantiated
	 * @param Form $form
	 */
	public function _setForm( Form $form ) {
		$this->_form = $form;
	}

	/**
	 * Displays the markup for a single element along with it's attributes
	 */
	public function render() {
		if ( isset( $this->_attributes['value'] ) && is_array( $this->_attributes['value'] ) ) {
			$this->_attributes['value'] = '';
		}

		echo '<input', $this->getAttributes(), '/>';
	}

	/**
	 * Simple getter method for fetching the elements label
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}

	/**
	 * Simple setter method for adding the elements label
	 * @param $label
	 */
	public function setLabel( $label ) {
		$this->label = $label;
	}

	/**
	 * Fetches the description on the element if one is set
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Checks if the current element needs validation
	 * @return bool
	 */
	public function isRequired() {
		if ( empty( $this->validation ) ) {
			return false;
		}

		foreach ( $this->validation as $validation ) {
			if ( $validation instanceof Validation\Required ) {
				return true;
			}
		}

		return false;
	}
}
