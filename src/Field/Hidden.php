<?php

namespace ODL\Field;

use ODL\Field;

/**
 * Class Hidden
 * @package ODL\Field
 */
class Hidden extends Field {

	/**
	 * Default attributes
	 * @var array
	 */
	protected $_attributes = [ 'type' => 'hidden' ];

	/**
	 * Hidden constructor.
	 *
	 * @param $name
	 * @param string $value
	 * @param array $props
	 */
	public function __construct( $name, $value = '', array $props = [] ) {
		if ( ! empty( $value ) ) {
			$props['value'] = $value;
		}

		parent::__construct( '', $name, $props );
	}
}