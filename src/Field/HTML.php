<?php

namespace ODL\Field;

use ODL\Field;

/**
 * Class HTML
 * @package ODL\Field
 */
class HTML extends Field {

	/**
	 * HTML constructor.
	 *
	 * @param $value
	 */
	public function __construct( $value ) {
		$props = [ 'value' => $value ];

		parent::__construct( '', '', $props );
	}

	/**
	 * Render the markup but pass it through wp_kses to ensure the data is secure
	 */
	public function render() {
		echo $this->_attributes['value'];
	}
}
