<?php

namespace ODL\Field;

use ODL\Field;

/**
 * Class Text
 * @package ODL\Field
 */
class Text extends Field {

	/**
	 * Define the default type attribute on the element
	 * @var array
	 */
	protected $_attributes = [ 'type' => 'text' ];

	/**
	 * Add stylized markup before the field
	 * @var
	 */
	protected $prepend;

	/**
	 * Add stylized markup after the field
	 * @var
	 */
	protected $append;

	/**
	 * Displays the output for the element
	 */
	public function render() {
		$addons = [];
		if ( ! empty( $this->prepend ) ) {
			$addons[] = 'input-prepend';
		}
		if ( ! empty( $this->append ) ) {
			$addons[] = 'input-append';
		}
		if ( ! empty( $addons ) ) {
			echo '<div class="', implode( ' ', $addons ), '">';
		}

		$this->renderAddOn( 'prepend' );
		parent::render();
		$this->renderAddOn( 'append' );

		if ( ! empty( $addons ) ) {
			echo '</div>';
		}
	}

	/**
	 * Renders the addons before or after the field
	 * @param string $type
	 */
	protected function renderAddOn( $type = 'prepend' ) {
		if ( empty( $this->$type ) ) {
			return;
		}

		$span = true;
		if ( false !== strpos( $this->$type, '<button' ) ) {
			$span = false;
		}

		if ( $span ) {
			echo '<span class="add-on">';
		}

		echo $this->$type;

		if ( $span ) {
			echo '</span>';
		}
	}
}
