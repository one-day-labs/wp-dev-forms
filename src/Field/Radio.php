<?php

namespace ODL\Field;

/**
 * Class Radio
 * @package ODL\Field
 */
class Radio extends Checkbox {

	/**
	 * Default attributes
	 * @var array
	 */
	protected $_attributes = [ 'type' => 'radio' ];
}