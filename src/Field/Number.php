<?php

namespace ODL\Field;

/**
 * Class Number
 * @package ODL\Field
 */
class Number extends Text {

	/**
	 * Default attributes
	 * @var array
	 */
	protected $_attributes = [ 'type' => 'number' ];
}