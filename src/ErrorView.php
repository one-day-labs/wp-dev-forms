<?php

namespace ODL;

/**
 * Class ErrorView
 * @package ODL
 */
abstract class ErrorView extends Base {

	/**
	 * The Form that this view is attached to
	 * @var Form
	 */
	protected $_form;

	/**
	 * ErrorView constructor.
	 *
	 * @param array|null $props
	 */
	public function __construct( array $props = null ) {
		$this->configure( $props );
	}

	/**
	 * Adds the Form this ErrorView is attached to
	 * @param Form $form
	 */
	public function _setForm( Form $form ) {
		$this->_form = $form;
	}

	/**
	 * @return mixed
	 */
//	public abstract function applyAjaxErrorResponse();

	/**
	 * @return mixed
	 */
	public abstract function render();

	/**
	 * @return mixed
	 */
//	public abstract function renderAjaxErrorResponse();

}
