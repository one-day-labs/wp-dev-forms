<?php
/**
 * Plugin Name: WP Dev Forms
 * Plugin URI:
 * Description: A WordPress developer toolkit that is an easy to use form builder for plugins and/or themes. This is
 * specifically for creating forms via CODE.
 * Version:     0.1.0
 * Author:      One Day Labs
 * Author URI:  https://onedaylabs.com
 * Text Domain: wpdf
 * Domain Path: /languages
 *
 * @package ODL
 */

require __DIR__ . '/vendor/autoload.php';