<?php
/**
 * Primary entry point to building the form. Instantiate this class into a variable and then add elements to it to
 * create your form.
 */

namespace ODL;

/**
 * Class Form
 * @package ODL
 */
class Form extends Base {

	/**
	 * The form fields that will be displayed in this form
	 * @var array
	 */
	protected $_fields = [];

	/**
	 * The values of our elements
	 * @var array
	 */
	protected $_values = [];

	/**
	 * Required from Base class
	 * @var array
	 */
	protected $_attributes = [];

	/**
	 * The View object this form should use
	 * @var View
	 */
	protected $view;

	/**
	 * The view that will process any errors
	 * @var View\Error
	 */
	protected $errorView;

	/**
	 * Tells the form to set any labels as placeholders (??)
	 * @var bool
	 */
	protected $setPlaceholder = false;

	/**
	 * Form constructor.
	 *
	 * @param string $id The ID we wish to assign to the form and used for fields
	 * @param array $values An array of values that should be populated into the form fields
	 * @param array $args An array of arguments that will be used for configuring the attributes & settings of the form
	 * @param string|null $view Pass as a string with the namespace and name of the class.
	 * e.g. '\\ODL\\View\\SideBySide'
	 */
	public function __construct( $id, $values = [], $args = [], $view = null ) {
		$default_args = [
			'id'     => sanitize_title_with_dashes( $id ),
			'action' => '',
			'method' => 'post',
			'nonce'  => 'nonce-' . sanitize_title_with_dashes( $id ), // It is recommended a nonce be passed in form
		];
		$args         = wp_parse_args( $args, $default_args );

		// Extract our nonce from the arguments so it isn't set as an attribute on the form
		$nonce = $args['nonce'];
		unset( $args['nonce'] );

		// Load the default view if one isn't set
		$this->view = ( empty( $view ) ) ? new View\SideBySide : new $view;

		// Load the default errorView
		if ( empty( $this->errorView ) ) {
			$this->errorView = new View\Error;
		}

		// Configure our form
		$this->configure( $args );

		// Set our values onto the form object
		$this->setValues( $values );

		// Load our action and nonce into the form first
		$action = str_replace( '-', '_', sanitize_key( 'save_' . $args['id'] ) );
		$this->addField( new Field\Hidden( $action, 1 ) );
		// Set a name as if this nonce is used within the admin area, using the default "_wpnonce" will cause duplicate
		// nonce's with the same name
		$this->addField( new Field\Nonce( sanitize_title_with_dashes( $nonce ), 'form-nonce-' . $args['id'] ) );
	}

	/**
	 * Adds a field object to the form by passing a specific field type object like Field\HTML, or Field\Text
	 *
	 * @param Field $field
	 */
	public function addField( Field $field ) {
		$field->_setForm( $this );

		// If the field doesn't have a specified ID, a generic ID will be applied
		if ( empty( $field->getAttribute( 'id' ) ) ) {
			$field->setAttribute( 'id', $this->_attributes['id'] . '-field-' . count( $this->_fields ) );
		}
		$this->_fields[] = $field;

		// If any field allows the uploading of a file, we'll automatically set the form to a multipart form
		if ( $field instanceof Field\File ) {
			$this->_attributes['enctype'] = 'multipart/form-data';
		}
	}

	/**
	 * Render the markup of our form and fields
	 *
	 * @param bool $returnHTML
	 *
	 * @return bool|false|string
	 */
	public function render( $returnHTML = false ) {
		// Convert any labels to placeholder attributes instead if form is configured this way
		if ( $this->setPlaceholder ) {
			foreach ( $this->_fields as $field ) {
				$label = $field->getLabel();

				if ( ! empty( $label ) ) {
					$field->setAttribute( 'placeholder', $label );
					$field->setLabel( '' ); // Reset the label so we don't display the HTML
				}
			}
		}

		// Set our views
		$this->view->_setForm( $this );
		$this->errorView->_setForm( $this );

		// Add our default values to the form elements if any are set
		$this->applyValues();

		// Allow users to return the HTML
		if ( $returnHTML ) {
			ob_start();
		}

		// Process the markup
		$this->view->render();

		if ( $returnHTML ) {
			$html = ob_get_contents();
			ob_end_clean();

			return $html;
		}

		return true;
	}

	/**
	 * Simple getter method that fetches the ErrorView object
	 * @return View\Error
	 */
	public function getErrorView() {
		return $this->errorView;
	}

	/**
	 * Checks the active session for any errors
	 * @TODO WordPress can't use sessions so we need an alternative solution
	 * @return array
	 */
	public function getErrors() {
		$errors = [];
		if ( '' === session_id() ) {
			return $errors[''] = [ 'Error: An active session is required for the form to function properly.' ];
		}

		$id = $this->_attributes['id'];
		if ( ! empty( $_SESSIONS['odl'][ $id ]['errors'] ) ) {
			$errors = $_SESSION['odl'][ $id ]['errors'];
		}

		return $errors;
	}

	/**
	 * Simple getter method that fetches all the fields attached to the Form
	 * @return array
	 */
	public function getFields() {
		return $this->_fields;
	}

	/**
	 * Stores an array of key => value for the form fields
	 *
	 * @param array $values
	 */
	public function setValues( array $values ) {
		$this->_values = array_merge( $this->_values, $values );
	}

	/**
	 * Loop through the values set on the form and add them to the appropriate field attached to the form
	 */
	protected function applyValues() {
		foreach ( $this->_fields as $fields ) {
			$name = $fields->getAttribute( 'name' );

			// Check if we are dealing with a multidimensional array for values
			preg_match( '/\[.*?]/', $name, $matches );
			if ( ! empty( $matches ) ) {
				$name = str_replace( ']', '', explode( '[', $name ) );
			}

			// Check if we have fields we can populate
			if ( is_array( $name ) && isset( $this->_values[ $name[0] ][ $name[1] ] ) ) {
				// Check if we are dealing with more than one array value, return the array instead of a fixed key
				if (
					is_array( $this->_values[ $name[0] ][ $name[1] ] ) &&
					! isset( $this->_values[ $name[0] ][ $name[1] ][0] )
				) {
					$value = $this->_values[ $name[0] ][ $name[1] ];
				} else {
					$value = $this->_values[ $name[0] ][ $name[1] ][0];
				}
				$fields->setAttribute( 'value', $value );
			} elseif ( ! is_array( $name ) && isset( $this->_values[ $name ] ) ) {
				$fields->setAttribute( 'value', $this->_values[ $name ] );
			}
		}
	}
}
