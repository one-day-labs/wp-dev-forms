<?php

namespace ODL\Field;

/**
 * Class URL
 * @package ODL\Field
 */
class URL extends Text {

	/**
	 * Define the default type attribute on the element
	 * @var array
	 */
	protected $_attributes = [
		'type'  => 'url',
		'class' => 'form-control',
	];

	/**
	 * Output the markup
	 */
	public function render() {
//		$this->validation[] = new \ODL\Validation\URL;
		parent::render();
	}
}