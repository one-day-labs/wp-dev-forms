<?php

namespace ODL\Field;

use ODL\OptionField;

/**
 * Class Checkbox
 * @package ODL\Field
 */
class Checkbox extends OptionField {

	/**
	 * Set our default attributes with a type of checkbox
	 * @var array
	 */
	protected $_attributes = [ 'type' => 'checkbox' ];

	/**
	 * Allows us to configure the label of a checkbox to be inline
	 * @var string|bool
	 */
	protected $inline;

	/**
	 * Render the markup for checkbox(es)
	 */
	public function render() {
		if ( isset( $this->_attributes['value'] ) && ! is_array( $this->_attributes['value'] ) ) {
			$this->_attributes['value'] = [ $this->_attributes['value'] ];
		} else {
			$this->_attributes['value'] = [];
		}

//		if ( '[]' !== substr( $this->_attributes['value'], -2 ) ) {
//			$this->_attributes['name'] .= '[]';
//		}

		// Apply the "inline" class to the label so it can be styled inline
		$class = 'form-check-input';
		if ( ! empty( $this->inline ) ) {
			$class .= ' inline';
		}

		$count = 0;
		foreach ( $this->options as $value => $text ) {
			$value = $this->getOptionValue( $value );
			$id = $this->_attributes['id'] . '-' . $count;
			$atts = [
				'id',
				'value',
				'checked',
				'required',
			];

			echo '<input type="hidden" id="', esc_attr( $id ), '-hidden" name="', $this->getAttribute( 'name' ), '" value="FALSE"',
				'/> ';
			echo '<input id="', esc_attr( $id ), '"', $this->getAttributes( $atts ), ' class="', $class, '" value="',
			esc_attr( $value ),	'"', checked( $value, $this->_attributes['value'][0] ), '/> ';
			echo '<label class="form-check-label" for="', $id, '">', esc_html( $text ), '</label>';
			++$count;
		}
	}
}