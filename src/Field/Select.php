<?php

namespace ODL\Field;

use ODL\OptionField;

/**
 * Class Select
 * @package ODL\Field
 */
class Select extends OptionField {

	/**
	 * Output the markup
	 */
	public function render() {
		if ( isset( $this->_attributes['value'] ) && ! is_array( $this->_attributes['value'] ) ) {
			$this->_attributes['value'] = array( $this->_attributes['value'] );
		}

		// Allow multiple values for the key
		if ( ! empty( $this->_attributes['multiple'] ) && '[]' !== substr( $this->_attributes['name'], -2 ) ) {
			$this->_attributes['name'] .= '[]';
		}

		echo '<select', $this->getAttributes( array( 'value', 'selected' ) ), '>';
		foreach ( $this->options as $value => $text ) {
			// Handle hierarchy with option groups
			if ( is_array( $text ) ) {
				if ( isset( $text['children'] ) ) {
					echo '<optgroup label="', esc_attr( $text['name'] ), '">';
					foreach ( $text['children'] as $childTerm ) {
						$this->optionField( $childTerm['term_id'], $childTerm['name'] );
					}
					echo '</optgroup>';
				} else {
					$this->optionField( $value, $text['name'] );
				}
			} else {
				$this->optionField( $value, $text );
			}
		}
		echo '</select>';
	}

	/**
	 * Output a single option field
	 * @param $value
	 * @param $text
	 */
	protected function optionField( $value, $text ) {
		echo '<option value="', esc_attr( $value ), '"';
		if ( is_array( $this->_attributes['value'] ) && in_array( $value, $this->_attributes['value'] ) ) {
			echo ' selected="selected"';
		} elseif ( is_array( $this->_attributes['value'] ) && array_key_exists( $value, $this->_attributes['value'] ) ) {
			echo ' selected="selected"';
		}
		echo '>', $text, '</option>';
	}

	/**
	 * Outputs a simple option field. Meant specifically for the subgroups title
	 * @param $text
	 */
	protected function optionSubGroupField( $text ) {
		echo '<option>', $text, '</option>';
	}
}