# WP Dev Forms

A WordPress developer toolkit that is an easy to use form builder for plugins and/or themes. This is specifically for creating forms via CODE.

> This plugin is in **beta** and is by no means perfect or production ready. 

This plugin is a heavily forked version of [php-form-builder-class](https://github.com/lkorth/php-form-builder-class) and has been adapted to work with WordPress.

## Requirements

* WordPress: 5.3+
* PHP: 7.0+

## Install

1. Download the plugin to your computer.
1. Upload the plugin to your `/wp-content/plugins/` directory.
1. Activate the plugin in the plugins screen.

## How to use

This plugin is built to simplify the development process when creating forms for both the admin area and the front-end of your theme.
Once this plugin is activated, you have access to the `\ODL` namespace which can be used within your plugins or theme.

Example front-end form
```php
// Initialize a form object
$form = new \ODL\Form( 'form-id', $array_of_values, [] );

// Create all the fields this form needs by passing in various Field objects
$form->addField( new \ODL\Field\Text( 'Business Name', 'business_name' ) );
$form->addField( new \ODL\Field\URL( 'Website', 'website' ) );
$form->addField( new \ODL\Field\Phone( 'Phone', 'phone' ) );
$form->addField( new \ODL\Field\Text( 'Street', 'street' ) );
$form->addField( new \ODL\Field\Text( 'City', 'city' ) );
$form->addField( new \ODL\Field\State( 'State', 'state' ) );
$form->addField( new \ODL\Field\Text( 'Zip', 'zip' ) );

// Tell the form object to render our output
$form->render();
```

Example form used in custom meta boxes in wp-admin
```php
// Initialize a form object, but set our Admin view
$form = new \ODL\Form( 'form-id', $array_of_values, [], '\\ODL\\View\\WPAdmin' );

// Create all the fields this form needs by passing in various Field objects
$form->addField( new \ODL\Field\Text( 'Business Name', 'business_name' ) );
$form->addField( new \ODL\Field\URL( 'Website', 'website' ) );
$form->addField( new \ODL\Field\Phone( 'Phone', 'phone' ) );
$form->addField( new \ODL\Field\Text( 'Street', 'street' ) );
$form->addField( new \ODL\Field\Text( 'City', 'city' ) );
$form->addField( new \ODL\Field\State( 'State', 'state' ) );
$form->addField( new \ODL\Field\Text( 'Zip', 'zip' ) );

// Tell the form object to render our output
$form->render();
```

## Documentation

```php
\ODL\Form( $id, $values = [], $args = [], $view = null );
```
The Form object is used to start a new form. A unique identifier is required, and you can passing various other parameters to further customize your form element.
### Parameters
**$id** | String | Required<br>
The ID of the form field

**$values** | Array | Default: empty array<br>
An array that matches the name attribute of each field. This is used to prefill values into each field in the form.

**$args** | Array | Default: empty array<br>
An array of arguments that will be used for changing the default attributes added to a form element (e.g. action, method). Also used to setting a custom nonce action.

**$view** | String | Default: null<br>
The Form object utilizes two default views, "Simple" or "Admin". When value is null, we use the "Simple" view for displaying our forms.

If you are building a form in wp-admin, you should pass `\\ODL\\View\\WPAdmin`.

In the future, we'll offer a solution for you to create your own custom views in your plugin or theme.

___

```php
\ODL\Field\{type}( $label, $name, $props = null )
```
There are a handful of different types of fields you can create, Text, Radio, HTML, etc (more on that below). Use these field object by newing them up within `$form->addField()` so they are added to your form.

### Parameters
**$label** | String | Required<br>
The form label you want to display with the field.

**$name** | String | Required<br>
The value to be placed in the name attribute of the form field.

**$props** | Array | Default: empty array<br>
Add any other custom attributes that should be added to your field (i.e. class, id, data types, etc).

### Field Types

**Field\Button( string $label = 'Submit', string $type = '', array $props = [] )** <br>
Outputs a Button field, `<button type="$type" {$props}>$label</button>`.

**Field\Checkbox( string $label, string $name, array $props = null )**<br>
Outputs a input field with a type of 'checkbox' `<input type="checkbox" name="$name" {$props} />`.

**Field\Email( string $label, string $name, array $props = null )**<br>
Outputs a text field with a type of 'email' `<input type="email" name="$name" {$props} />`

**Field\File( string $label, string $name, array $props = null )**<br>
Outputs a text field with a type of 'file' `<input type="file" name="$name" {$props} />`

**Field\Hidden( string $label, string $name, array $props = null )**<br>
Outputs a text field with a type of 'hidden' `<input type="hidden" name="$name" {$props} />`

**Field\HTML( string $value )**<br>
Allows you to add any HTML you need to display within the form. All HTML passed does go through `wp_kses()` for security needs.

**Field\Nonce( string $action, string $name )**<br>
Set a custom [WordPress nonce field](https://developer.wordpress.org/reference/functions/wp_nonce_field/) by passing the `$action` and `$name`.<br>
By default a nonce is already created when you initialize a form so you shouldn't need this unless you need another nonce for whatever reason.

**Field\Number( string $label, string $name, array $props = null )**<br>
Outputs a text field with a type of 'number' `<input type="number" name="$name" {$props} />`

**Field\Phone( string $label, string $name, array $props = null )**<br>
Outputs a text field with a type of 'tel' `<input type="tel" name="$name" {$props} />`

**Field\Radio( string $label, string $name, array $props = null )**<br>
Outputs a text field with a type of 'radio' `<input type="radio" name="$name" {$props} />`

**Field\Select( string $label, string $name, array $options, array $props = null )**<br>
Outputs a select field and outputs an option field for each row in the `$options` array.  

**Field\State( string $label, string $name, array $props = null )**<br>
Similar to the select field but doesn't have an `$options` parameter as the list only displays a list of states in the USA.

**Field\Text( string $label, string $name, array $props = null )**<br>
Outputs a text field `<input type="text" name="$name" {$props} />`

**Field\Textarea( string $label, string $name, array $props = null )**<br>
Outputs a Textarea `<textarea name="$name" rows="5">$label</textarea>`

**Field\URL( string $label, string $name, array $props = null )**<br>
Outputs a text field with a type of 'url' `<input type="url" name="$name" {$props} />`

## Saving Form Data
This plugin only cares about creating forms and ensuring they are submitted. Saving will need to be handled on your end with some key information needed to do so.

1.