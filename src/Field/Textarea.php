<?php

namespace ODL\Field;

use ODL\Field;

/**
 * Class Textarea
 * @package ODL\Field
 */
class Textarea extends Field {

	/**
	 * Default attributes
	 * @var array
	 */
	protected $_attributes = [ 'rows' => '5' ];

	/**
	 * Render the markup
	 */
	public function render() {
		echo '<textarea', $this->getAttributes( 'value' ), '>';
		if ( ! empty( $this->_attributes['value'] ) ) {
			echo esc_html( $this->_attributes['value'] );
		}
		echo '</textarea>';
	}
}