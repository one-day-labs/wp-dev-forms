<?php


namespace ODL\Field;


class Date extends Text {
	/**
	 * @var array
	 */
	protected $_attributes = [
		'type' => 'date',
	];

	/**
	 * Date constructor.
	 *
	 * @param $label
	 * @param $name
	 * @param array|null $props
	 */
	public function __construct( $label, $name, array $props = null ) {
		$this->_attributes['placeholder'] = 'YYYY-MM-DD (e.g. ' . date( 'Y-m-d' ) . ')';
		$this->_attributes['title'] = $this->_attributes['placeholder'];

		parent::__construct( $label, $name, $props );
	}

	/**
	 * Render the output
	 */
	public function render() {
		parent::render();
	}
}