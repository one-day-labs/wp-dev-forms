<?php

namespace ODL\Field;

/**
 * Class Phone
 * @package ODL\Field
 */
class Phone extends Text {

	/**
	 * Default attributes
	 * @var array
	 */
	protected $_attributes = [ 'type' => 'tel' ];
}