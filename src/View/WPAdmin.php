<?php

namespace ODL\View;

use ODL\View;
use ODL\Field;
use ODL\Field\Hidden;
use ODL\Field\Nonce;
use ODL\Field\HTML;
use ODL\Field\Button;
use ODL\Field\Checkbox;

/**
 * Class WPAdmin
 * @package ODL\View
 */
class WPAdmin extends View {

	/**
	 * Default classes to apply to the form field on render
	 * @var string
	 */
	protected $class = 'form-table';

	/**
	 * Renders all the markup, assets, etc
	 */
	public function render() {
		$this->_form->setAttribute( 'class', $this->class );

		echo '<table', $this->_form->getAttributes( 'method' ), '><tbody>';

		// Display any form validation errors
//		$this->_form->getErrorView()->render();

		$fields = $this->_form->getFields();
		$total = count( $fields );
		$count = 0;
		for ( $i = 0; $i < $total; ++$i ) {
			$field = $fields[ $i ];

			if ( $field instanceof Hidden || $field instanceof Nonce ) {
				$field->render();
			} elseif ( $field instanceof HTML ) {
				echo '<tr><td colspan="2">', $field->render(), '</td></tr>';
			} elseif ( $field instanceof Button ) {
				if ( 0 === $i || ! $fields[ ( $i - 1 ) ] instanceof Button ) {
					echo '<div class="form-actions">';
				} else {
					echo '';
				}

				$field->render();

				if ( ( $i + 1 ) === $count || ! $fields[ ( $i + 1 ) ] instanceof Button ) {
					echo '</div>';
				}
			} else {
				echo '<tr id="field_', sanitize_title_with_dashes( $field->getAttribute('id' ) ), '">',
					'<th scope="row">', $this->renderLabel( $field ), '</th>',
					'<td>', $field->render(), $this->renderDescription( $field ), '</td>',
					'</tr>';
				++$count;
			}
		}

		echo '</tbody></table>';
	}

	/**
	 * Renders the output for the label of a single field
	 *
	 * @param Field $field
	 */
	protected function renderLabel( Field $field ) {
		$label = $field->getLabel();
		if ( empty( $label ) ) {
			return;
		}

		echo '<label for="', esc_attr( $field->getAttribute( 'id' ) ), '">';

		if ( $field->isRequired() ) {
			echo '<span class="required">*</span>';
		}

		echo esc_html( $label ), '</label>';
	}
}
