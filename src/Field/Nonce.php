<?php

namespace ODL\Field;

/**
 * Class Nonce
 * @package ODL\Field
 */
class Nonce extends Hidden {

	/**
	 * Return a WordPress nonce for enhanced security. This will pass in the name that was passed to the object.
	 */
	public function render() {
		return wp_nonce_field( $this->_attributes['name'], $this->_attributes['value'] );
	}
}