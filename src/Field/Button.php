<?php

namespace ODL\Field;

use ODL\Field;

/**
 * Class Button
 * @package ODL\Field
 */
class Button extends Field {

	/**
	 * Default attributes
	 * @var array
	 */
	protected $_attributes = [
		'type' => 'submit',
		'value' => 'Submit',
	];

	/**
	 * Icon to display within the button
	 * @var string
	 */
	protected $icon;

	/**
	 * Button constructor.
	 *
	 * @param string $label
	 * @param string $type
	 * @param array $props
	 */
	public function __construct( $label = 'Submit', $type = '', array $props = [] ) {
		if ( ! empty( $type ) ) {
			$props['type'] = $type;
		}

		// Add default class to the button. If it is a submit button, add the primary class to highlight it
		$class = 'btn';
		if ( empty( $type ) || $type === 'submit' ) {
			$class .= ' btn-primary';
		}

		// Append or add our class to the properties
		if ( ! empty( $props['class'] ) ) {
			$props['class'] .= ' ' . $class;
		} else {
			$props['class'] = $class;
		}

		// Add the value if one is set.
		if ( empty( $props['value'] ) )  {
			$props['value'] = $label;
		}

		parent::__construct( '', '', $props );
	}
}