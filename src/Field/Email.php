<?php

namespace ODL\Field;

/**
 * Class Email
 * @package ODL\Field
 */
class Email extends Text {

	/**
	 * Default attributes
	 * @var array
	 */
	protected $_attributes = [ 'type' => 'email' ];
}