<?php

namespace ODL\View;

use ODL\View;
use ODL\Field;
use ODL\Field\Hidden;
use ODL\Field\Nonce;
use ODL\Field\HTML;
use ODL\Field\Button;
use ODL\Field\Checkbox;

/**
 * Class SideBySide
 * @package ODL\View
 */
class SideBySide extends View {

	/**
	 * Default classes to apply to the form field on render
	 * @var string
	 */
	protected $class = 'form-horizontal';

	/**
	 * Renders all the markup, assets, etc
	 */
	public function render() {
		$this->_form->appendAttribute( 'class', $this->class );

		echo '<form', $this->_form->getAttributes(), '><fieldset>';

		// Display any form validation errors
//		$this->_form->getErrorView()->render();

		$fields = $this->_form->getFields();
		$total = count( $fields );
		$count = 0;
		for ( $i = 0; $i < $total; ++$i ) {
			$field = $fields[ $i ];

			if (
				$field instanceof Hidden ||
				$field instanceof Nonce ||
				$field instanceof HTML
			) {
				$field->render();
			} elseif ( $field instanceof Button ) {
				if ( 0 === $i || ! $fields[ ( $i - 1 ) ] instanceof Button ) {
					echo '<div class="form-actions">';
				} else {
					echo '';
				}

				$field->render();

				if ( ( $i + 1 ) === $count || ! $fields[ ( $i + 1 ) ] instanceof Button ) {
					echo '</div>';
				}
			} elseif ( $field instanceof Checkbox ) {
				echo '<div class="form-check" id="field_',
					sanitize_title_with_dashes( $field->getAttribute('id') ), '">',
					$field->render(),
					$this->renderDescription( $field ),
					'</div>';
				++$count;
			} else {
				echo '<div class="form-group" id="field_',
					sanitize_title_with_dashes( $field->getAttribute('id' ) ), '">',
					$this->renderLabel( $field ),
					$field->render(),
					$this->renderDescription( $field ),
					'</div>';
				++$count;
			}
		}

		echo '</fieldset></form>';
	}

	/**
	 * Renders the output for the label of a single field
	 *
	 * @param Field $field
	 */
	protected function renderLabel( Field $field ) {
		$label = $field->getLabel();
		if ( empty( $label ) ) {
			return;
		}

		echo '<label class="control-label" for="', esc_attr( $field->getAttribute( 'id' ) ), '">';

		if ( $field->isRequired() ) {
			echo '<span class="required">*</span>';
		}

		echo esc_html( $label ), '</label>';
	}
}
