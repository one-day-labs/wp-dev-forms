<?php

namespace ODL\View;

use ODL\ErrorView;

/**
 * Class Error
 * @package ODL\View
 */
class Error extends ErrorView {

	/**
	 * Parses the errors
	 * @param $errors
	 *
	 * @return array
	 */
	public function parse( $errors ) {
		$list = [];

		if ( empty( $errors ) ) {
			return $list;
		}

		$keys = array_keys( $errors );
		$keyCount = count( $keys );
		for ( $k = 0; $k < $keyCount; ++$k ) {
			$list = array_merge( $list, $errors[ $keys[ $k ] ] );
		}

		return $list;
	}

	/**
	 * Renders any errors found within the form
	 * @return mixed|void
	 */
	public function render() {
		$errors = $this->parse( $this->_form->getErrors() );

		if ( empty( $errors ) ) {
			return;
		}

		$count = count( $errors );
		$errors = implode( '</li><li>', $errors );
		$format = ( 1 === $count ) ? 'error was' : $count . ' errors were';

		echo <<<HTML
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert">x</a>
	<strong class="alert-heading">The following $format found:</strong>
	<ul><li>$errors</li></ul>		
</div>
HTML;
	}
}
